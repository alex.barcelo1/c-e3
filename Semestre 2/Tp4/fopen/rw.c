#include <stdio.h>

int main(int argc, char const *argv[])
{
    int i;
    /*if fopen(...) == NULL: fclose(...)*/
    FILE* rwd = fopen("monfichier.txt","w+");/*w+ crée le fichier s'il n'existe pas et l'efface s'il existe*/
    fprintf(rwd,"hello sera la seule ligne peu importe le nombre d'utilisation de ce programme");
fclose(rwd);

    FILE* rwo = fopen("myfile.txt","r+");/*r+ n'efface pas le fichier mais écrit depuis la position 0 donc on écrit par dessus l'ancien texte*/
    fprintf(rwo, "Hello");
fclose(rwo);

    FILE* rwa = fopen("txt.txt", "a");
    fprintf(rwa, "Ce programme à été utilisé une fois par ligne\n");
    for (i=0;i<5;i++){
        fprintf(rwa,"i=%d \n",i);
    }
fclose(rwa);

    return 0;
}
