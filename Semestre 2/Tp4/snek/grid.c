#include "grid.h"

char grid[NBL][NBC] = {
    "w                                  w",
    "                                    ",
    "               f                    ",
    "                                    ",
    "     f               f              ",
    "                                    ",
    "                                    ",
    "               f                    ",
    "                                    ",
    "                                    ",
    "         wwwwwwwwww                 ",
    "                                    ",
    "                                    ",
    "                                    ",
    "                                    ",
    "                                    ",
    "                  f                 ",
    "                                    ",
    "         f                f         ",
    "                                    ",
    "                 f                  ",
    "w                                  w"
};

void debug(char m[NBL][NBC]){
	int i,j;
	for (i = 0; i < NBL; i++)
	{
        printf("[");
		for (j = 0; j < NBC; j++)
		{
			printf("%c",m[i][j]);
			if(j==NBC-1){
				printf("]");
                printf("\n");
			}
		}
		
	}
	
}

int compute_size(/*int w, int h*/){
    /*int m1 = w/L;
    int m2 = h/(C-1);*/
    int m1 = X/(NBL);
    int m2 = Y/NBC;
    if (m1<m2){
        return m1;
    }else{
        return m2;
    }
}

/*void log(char* fname, char g[NBL][NBC]){
    FILE* f = fopen(fname,"a");
    char buf[NBC];
    while(fgets(buf,NBC,))
}*/

tiles move_snake(Snake* s, char grille[NBL][NBC]){
    int tailx = s->body[SNAKE_SIZE-1].x;
    int taily = s->body[SNAKE_SIZE-1].y;
    int hx,hy;
    grille[tailx][taily] = EMPTY;
    crawl(s/*,&hx,&hy*/);
    hx=s->body[0].x;
    hy=s->body[0].y;
    return grille[hx][hy];
}


void draw_grid(char grille[NBL][NBC], int a){ /*a=compute_size*/
    
    int i,j;
    DFREC(0,0,X,Y,MLV_COLOR_WHITE);
    if (vdebug) {printf ("grille : \n");debug(grille);}
    /*if (strcmp(vimprim,NULL)){log(vimprim,grille);}*/
    for(i=0;i<NBL;i++){
        for(j=0;j<NBC;j++){
            MLV_Color col = MLV_COLOR_BLACK;
            switch (grille[i][j])
            {
                case WALL:
                    col = MLV_COLOR_BROWN4;
                    break;
                case FRUIT:
                    col = MLV_COLOR_RED1;
                    break;
                case EMPTY:
                    col = MLV_COLOR_GREEN1;
                    break;
                case SNAKE:
                    col = MLV_COLOR_YELLOW1;
                    break;
                default:
                    break;
            }
            DFREC(i*a,j*a,a,a,col);
        }
    }
    

}