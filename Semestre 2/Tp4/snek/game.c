#include "grid.h"
#define FPS 300 /*PERIODE de rafraichissement*/
#define WIN_SCORE 1

const char* program_name;
int vdebug = 0; 
char vimprim[] = "";

void print_help(){
    printf("Jeu Snake E3 C\n-h --help : pour ce message\n");
    printf("-i --ingrid PATH_TO_FILE : pour jouer une grille perso\n");
    printf("-d --debug : pour print des infos relatives à la partie en cours\n/!\\Beaucoup d'infos vont être print, il est conseillé de lancer le jeu avec ./game.out -d > log\n");
}

int compte_fruit(){
    int i,j;
    int c=0;
    for (i=0; i<NBL; i++){
        for(j=0; j<NBC; j++){
            if (grid[i][j] == FRUIT){
                if(vdebug){printf("Fruit en %d-%d\n",i,j);}
                c++;
            }
        }
    }
    printf("%d",c);
    return c;
}

void gg(){
    int xsmile[4] = {X/7+X/30,X/7+X/100,X/7-X/100,X/7-X/30};
    int ysmile[4] = {Y/7+Y/30,Y/7+2*Y/30,Y/7+2*Y/30,Y/7+Y/30};
    int ysmile2[4] = {Y/7+Y/30,Y/7+3*Y/30,Y/7+3*Y/30,Y/7+Y/30};
  
    MLV_draw_filled_circle(X/7,Y/7,X/10,MLV_COLOR_YELLOW1);
    MLV_draw_filled_circle(X/7+X/30,Y/7-Y/30,X/50,MLV_COLOR_RED);
    MLV_draw_filled_circle(X/7-X/30,Y/7-Y/30,X/50,MLV_COLOR_RED);
    MLV_draw_bezier_curve(xsmile,ysmile,4,MLV_COLOR_BLACK);
    MLV_draw_bezier_curve(xsmile,ysmile2,4,MLV_COLOR_BLACK);
}

void lose(int e){
    MLV_Keyboard_button touche;
    char* lmess ;
    if (e == 'w'){lmess = "Tu as mangé un mur - esc pour quitter";}
    if (e == 's'){lmess = "Tu as mangé ta queue - esc pour quitter";}
    if (e == 'f'){lmess = "Félicitations voici ton invitation pour rejoindre le FaZe Clan";gg();}
    MLV_draw_text(X/10,Y/2,lmess,MLV_COLOR_BLACK);
    MLV_actualise_window();
    while(1){
        MLV_get_event(&touche,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
        if (touche == MLV_KEYBOARD_ESCAPE){
            exit(0);
        }
    }
}

void place_snake(char g[NBL][NBC], Snake s){
    int i;
    int sx,sy;
    for(i=0;i<SNAKE_SIZE;i++){
        sx = s.body[i].x;
        sy = s.body[i].y;
        g[sx][sy] = SNAKE;
    }
}

int main(int argc, char* argv[])
{
    MLV_Keyboard_button touche;
    int cloop = 0;
    int a;
    Snake s;
    int next_option;
    tiles eat;
    int perdu=0;
    int win=compte_fruit()/*WIN_SCORE*/;
    /*DEBUT GESTION OPTIONS*/  
    const char* short_opt="hdi:";
    const struct option long_opt[] = {
        {"help",0,NULL,'h'},
        {"debug",0,NULL,'d'},
        {"ingrid", 1, NULL, 'i'},
        { NULL, 0, NULL, 0} 
    };
    program_name=argv[0];
    next_option=0;
    while (next_option != -1)
    {
        next_option=getopt_long(argc,argv,short_opt,long_opt,NULL);
        switch (next_option){
        case 'h':
            print_help();
            return 0;
        case 'd':
            vdebug = 1;
            break;
        case 'i':
            strcpy(vimprim,optarg);
            break;
        default:
            break;
        }
    }
    /*FIN GESTION OPTIONS*/

    /*CHOIX GRILLE*/
    if (strcmp(vimprim,"") != 0){
        int ig;
        char buf[NBC+1];
        FILE* fg = fopen(vimprim,"r");
        if(fg == NULL){
            printf("File not found, utilisation de la grille par défaut \n");
        }
        else{
            for (ig = 0; ig<NBL; ig++){
                fgets(buf, NBC+2, fg); /*+2 pour que fgets voit le \n et passe à la ligne suivante*/
                strcpy(grid[ig],buf);
            }
            fclose(fg);
        }
    }
    /*FIN CHOIX GRILLE*/

    /*Snake s = {{1,3},{1,2},{1,1},{1,0}};*/
    int i;
    int posS[8] = {1,3,1,2,1,1,1,0};
    for(i=0;i<SNAKE_SIZE;i++){
        s.body[i].x=posS[i*2];
        s.body[i].y=posS[i*2+1];
    }
    s.next_dir=TOP;
    place_snake(grid,s);
    /*FIN LOGIQUE SNAKE*/

    MLV_create_window("Fenetre dessin","Icone dessin",X,Y);
    
    a = compute_size(/*X,Y*/);
    while(1){
        
            
        MLV_get_event(&touche,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
        if (touche == MLV_KEYBOARD_ESCAPE){
            break;
        }else if(!perdu){
            set_dir(touche,&s);
        }
        if (cloop%FPS == 0){
            eat = move_snake(&s,grid);
            if (eat == FRUIT){win--;}
        }
        if((eat!=EMPTY && eat!=FRUIT) || (eat==FRUIT && win==0)){lose(eat);}
        place_snake(grid,s);
        draw_grid(grid,a);
        MLV_actualise_window();       
        
        cloop = (cloop+1)%FPS;
    }

/*  
    while (1)
    {
        MLV_wait_keyboard(&touche, NULL, NULL);
        if (touche == MLV_KEYBOARD_ESCAPE){
            break;
        }
        
    }*/

    return 0;
}
