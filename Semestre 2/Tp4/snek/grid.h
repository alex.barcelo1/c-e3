#ifndef fGRID
#define fGRID

#include "snake.h"
#define MAX_FILENAME_SIZE 100

extern char vimprim[MAX_FILENAME_SIZE];
extern char grid[NBL][NBC];
typedef enum tiles {WALL='w',EMPTY=' ',FRUIT='f',SNAKE='s'}tiles;
void debug(char grille[NBL][NBC]);
int compute_size(/*int w, int h*/);
void draw_grid(char grille[NBL][NBC], int a);
tiles move_snake(Snake* s, char grille[NBL][NBC]);

#endif