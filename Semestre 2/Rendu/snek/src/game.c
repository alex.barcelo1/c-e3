#define _GNU_SOURCE
#include "grid.h"
#define FPS 200 /*PERIODE de rafraichissement*/
#define WIN_SCORE 1 /*plus utilisé*/
#define CONTINUE 20000

const char* program_name;
int vdebug = 0; 
int vsound = 0;
char vimprim[] = "";

void print_help(){
    printf("Jeu Snake E3 C\n-h --help : pour ce message\n");
    printf("-i --ingrid PATH_TO_FILE : pour jouer une grille perso\n");
    printf("-d --debug : pour print des infos relatives à la partie en cours\n/!\\Beaucoup d'infos vont être print, il est conseillé de lancer le jeu avec ./game.out -d > log\n");
    printf("-s --sound : pour jouer avec les sons\n/!\\ EXTREMEMENT RIGOLO\n");
}

void stripcp(char* source, char * cible){/*Copie source dans cible jusqu'à la première occurence d'un \0 ou d'un \n qui ne sera pas copié*/
    int i;
    for(i=0;source[i] != '\0' && source[i] != '\n';i++){
        cible[i]=source[i];
    }
}

int compte_fruit(Grid* g){
    int i,j;
    int c=0;
    for (i=0; i<g->snbl; i++){
        for(j=0; j<g->snbc; j++){
            /*if(vdebug){printf("i,j = %d,%d\n",i,j);}*/
            if (g->sgrid[i][j] == FRUIT){                
                if(vdebug){printf("Fruit en %d-%d\n",i,j);}
                c++;
            }
        }
    }
    if(vdebug){printf("%d fruits sur la grille\n", c);}
    return c;
}

int compte_ligne(FILE* f){
    int ret=0;
    char ch;
    while(!feof(f)){
        ch = fgetc(f);
        if(ch == '\n'){
            ret++;
        }
    }
    return ret;/*la dernière ligne n'a pas de \n */
}

void gg(){
    int xsmile[4] = {X/7+X/30,X/7+X/100,X/7-X/100,X/7-X/30};
    int ysmile[4] = {Y/7+Y/30,Y/7+2*Y/30,Y/7+2*Y/30,Y/7+Y/30};
    int ysmile2[4] = {Y/7+Y/30,Y/7+3*Y/30,Y/7+3*Y/30,Y/7+Y/30};
  
    MLV_draw_filled_circle(X/7,Y/7,X/10,MLV_COLOR_YELLOW1);
    MLV_draw_filled_circle(X/7+X/30,Y/7-Y/30,X/50,MLV_COLOR_RED);
    MLV_draw_filled_circle(X/7-X/30,Y/7-Y/30,X/50,MLV_COLOR_RED);
    MLV_draw_bezier_curve(xsmile,ysmile,4,MLV_COLOR_BLACK);
    MLV_draw_bezier_curve(xsmile,ysmile2,4,MLV_COLOR_BLACK);
}

void play_sound(char* ps){
    int i;
    for (i = 0; ps[i] !='\0'; i++){
        if(ps[i] == ';' || ps[i] == '|' || ps[i] == '&'){exit(1);}
    }/*Sécu*/
    
    if (vsound){
        /*Appel à système après utilisation d'une string sans vérif de taille n'est ici pas une faille car la string en question n'est pas une input de l'utilisateur*/
        char s[50];
        sprintf(s,"aplay sounds/%s",ps);
        system(s);
    }
}

void lose(int e, Grid* sgrid, lSnake* ls){
    MLV_Keyboard_button touche;
    char* lmess;
    char score[50];
    if (e == 'w'){lmess = "Tu as mangé un mur - esc pour quitter";play_sound("sad.wav");}
    if (e == 's'){lmess = "Tu as mangé ta queue - esc pour quitter";play_sound("sad.wav");}
    if (e == 'f'){lmess = "Félicitations voici ton invitation pour rejoindre le FaZe Clan";gg();play_sound("mlg.wav");}
    sprintf(score,"Tu as marqué %d points !",ls->size);
    MLV_draw_text(X/10,Y/2,lmess,MLV_COLOR_BLACK);
    MLV_draw_text(X/10,Y/3,score,MLV_COLOR_BLACK);
    MLV_actualise_window();
    while(1){
        MLV_get_event(&touche,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
        if (touche == MLV_KEYBOARD_ESCAPE){
            free_grid(sgrid);
            free_snake(ls);
            exit(0);
        }
    }
}

void spawn_continue(Grid* sgrid){
    int x,y;
    time_t t;
    srand((unsigned) time(&t));
    x=rand()%sgrid->snbl;
    y=rand()%sgrid->snbc;
    while(sgrid->sgrid[x][y] != EMPTY){
        x=rand()%sgrid->snbl;
        y=rand()%sgrid->snbc;    
    }
    sgrid->sgrid[x][y] = eCONTINUE;
    if(vdebug) printf("Petit cadeau en %d %d\n",x,y);
}

void place_snake(Grid* g, lSnake* ls){
    int sx,sy;
    lCoord* current = ls->seg_list;
    while(current != NULL){
        sx = current->x;
        sy = current->y;
        g->sgrid[sx][sy] = SNAKE;
        current = current->next;
    }
}

/*void test_lSnake(){
    lSnake* test = new_snake();
    add_fin(test, 10, 10);
    add_fin(test,1,2);
    int i;
    for(int i = 0 ; i<5 ; i++){
        add_fin(test, i, i*10);
    }
    print_snake(test);
    print_snake(test);
    free_snake(test);
}*/

int main(int argc, char* argv[])
{
    MLV_Keyboard_button touche;
    int cloop = 0;
    int a;
    int win;
    int i;
    int ig;
    int nblf,nbcf;
    int perdu=0;
    int next_option;
    int vcontinue=1;
    int ncontinue=0;
    int scontinue=0;
    int posS[8] = {1,3,1,2,1,1,1,0};
    char* buf=NULL;
    size_t size_buf, size_tmp;
    lSnake* ls;
    tiles eat;
    Grid* vsgrid;
    FILE* fg;
    /*DEBUT GESTION OPTIONS*/  
    const char* short_opt="hdi:s";
    const struct option long_opt[] = {
        {"help",0,NULL,'h'},
        {"debug",0,NULL,'d'},
        {"ingrid", 1, NULL, 'i'},
        {"sound", 0,NULL, 's'},
        { NULL, 0, NULL, 0} 
    };
    program_name=argv[0];
    next_option=0;
    while (next_option != -1)
    {
        next_option=getopt_long(argc,argv,short_opt,long_opt,NULL);
        switch (next_option){
        case 'h':
            print_help();
            return 0;
        case 'd':
            vdebug = 1;
            break;
        case 'i':
            strcpy(vimprim,optarg);
            break;
        case 's':
            vsound = 1;
            break;
        default:
            break;
        }
    }
    /*test_lSnake();exit(0);*/
    /*FIN GESTION OPTIONS*/

    /*CHOIX GRILLE*/

    if (strcmp(vimprim,"") == 0){printf("Aucun level passé utilisation de levels/default \n");strcpy(vimprim,"levels/default");}

    fg = fopen(vimprim,"r");
    if(fg == NULL){
        printf("File not found, utilisation de la grille par défaut \n");
        fg = fopen("levels/default","r");
    }
    nblf = compte_ligne(fg);
    rewind(fg);
    nbcf = getline(&buf,&size_buf,fg);
    if(nbcf==-1){
        printf("erreur dans le fichier : %s",vimprim);
        exit(1);
    }
    else nbcf--;
    vsgrid = allocate_grid(nblf,nbcf);
    stripcp(buf,vsgrid->sgrid[0]);
    /*buf = malloc(nbcf+1);*/
    /*char buf[vsgrid->snbc+1];*/
    for (ig = 1; ig < nblf; ig++){
        size_tmp = getline(&buf,&size_buf,fg);        
        if((int)size_tmp != nbcf+1){
            printf("Erreur : La ligne %d à %ld chars alors que la 1ère en à %d",ig+1,size_tmp,nbcf-1);
            exit(1);
        }
        stripcp(buf,vsgrid->sgrid[ig]);
    }
    if (vdebug){printf("Dimension de la grille lue (%s) %d - %d\n",vimprim,nblf,nbcf);}
    free(buf);
    fclose(fg);
        
    
    /*FIN CHOIX GRILLE*/

    win=compte_fruit(vsgrid)/*WIN_SCORE*/;
    /*DEBUT LOGIQUE SNAKE Snake s = {{1,3},{1,2},{1,1},{1,0}};*/
    ls = new_snake();
    ls->size = SNAKE_INITIAL_SIZE;
    for(i=0;i<SNAKE_INITIAL_SIZE;i++){
        add_fin(ls, posS[i*2], posS[i*2+1]);
    }
    place_snake(vsgrid,ls);
    /*FIN LOGIQUE SNAKE*/

    MLV_create_window("Fenetre dessin","Icone dessin",X,Y);
    
    a = compute_size(vsgrid->snbl,vsgrid->snbc);
    /*BOUCLE DE JEU*/
    while(1){
        
            
        MLV_get_event(&touche,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
        if (touche == MLV_KEYBOARD_ESCAPE){
            break;
        }else if(!perdu){
            set_dir(touche,ls);
        }
        if (cloop%FPS == 0){
            if (vdebug) {printf ("grille : \n");debug(vsgrid);}
            eat = move_snake(ls,vsgrid);
            if (eat == FRUIT){win--;}
            if (eat == eCONTINUE){
                ncontinue++;
                scontinue=0;
                play_sound("nice.wav");
                printf("Tu as %d continues tu peut eviter %d murs\n",ncontinue,ncontinue);
            }
            if((eat == WALL || eat == SNAKE) && ncontinue > 0){
                printf("Malheureusement tu viens de manger un obstacle, heureusement tu avais un continue !\n");
                eat=EMPTY;
                ncontinue--;
                play_sound("oof.wav");
            }
        }
        if((eat!=EMPTY && eat!=FRUIT && eat!=eCONTINUE) || (eat==FRUIT && win==0)){
            lose(eat,vsgrid,ls);
        }
        if(vcontinue%CONTINUE == 0 && !scontinue){
            spawn_continue(vsgrid);
            scontinue = 1;
        }
        place_snake(vsgrid,ls);
        draw_grid(vsgrid,a);
        MLV_actualise_window();       
        
        cloop = (cloop+1)%FPS;
        vcontinue = (vcontinue+1)%CONTINUE; 
    }
    
    free_grid(vsgrid);
    free_snake(ls);
/*  
    while (1)
    {
        MLV_wait_keyboard(&touche, NULL, NULL);
        if (touche == MLV_KEYBOARD_ESCAPE){
            break;
        }
        
    }*/

    return 0;
}
