#ifndef fGRID
#define fGRID

#include "snake.h"
#define MAX_FILENAME_SIZE 100

typedef struct _grid
{
    char ** sgrid;
    int snbl;
    int snbc;
}Grid;

extern char vimprim[MAX_FILENAME_SIZE];
/*extern char grid[NBL][NBC];*/
typedef enum tiles {WALL='w',EMPTY=' ',FRUIT='f',SNAKE='s',eCONTINUE='c'}tiles;
Grid* allocate_grid(int n, int m);
void free_grid(Grid* g);
void debug(Grid* g);
int compute_size(int nbl,int nbc);
void draw_grid(Grid* g, int a);
tiles move_snake(lSnake* ls, Grid* g);

#endif