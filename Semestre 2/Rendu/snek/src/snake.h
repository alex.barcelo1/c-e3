#ifndef fSNAKE
#define fSNAKE

/*Tous les define/include sont ici pour eviter les dependances cycliques et garder show_snake dans snake.c (à cause de NBC et NBL)*/
/*#define NBL 22
#define NBC 36*/
#define X 440
#define Y 720
#define SNAKE_INITIAL_SIZE 4
/*#define T 1000*/
#define DFREC MLV_draw_filled_rectangle
#define DREC MLV_draw_rectangle

#include <MLV/MLV_all.h>
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdio.h>



extern int vdebug ;
typedef enum dir {TOP,BOTTOM,LEFT,RIGHT}dir;


typedef struct _lcoord
{
    int x;
    int y;
    struct _lcoord* next;
}lCoord;


typedef struct _lsnake
{
    lCoord* seg_list;
    int size;
    dir next_dir;
}lSnake;

void crawl(lSnake* s,int nbl,int nbc);
void set_dir(MLV_Keyboard_button t,lSnake* s);
void grandit(lSnake* ls,int hx,int hy);
lSnake* new_snake();
void add_fin(lSnake * ls, int x, int y);
void print_snake(lSnake* ls);
void free_snake(lSnake* ls);
#endif