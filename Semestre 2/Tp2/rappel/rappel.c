#include <stdio.h>

void max(int* t,int s){
    int max=t[0];
    int i;
    for(i=1;i<s;i++){
        if (t[i]>max)
        {
            max=t[i];
        }
    }
    printf("%d\n",max);
}

void copie(int* s,int* c,int ts, int tc){
    int i;
    if (tc<ts){printf("Attention le tableau cible est trop petit, le tableau source sera copié partiellement\n");}
    if (tc>ts){printf("Attention le tableau source est trop petit, le tableau cible sera que partiellement une copie de la source\n");}
    for(i=0;i<ts&&i<tc;i++){
        c[i]=s[i];
        /*printf("i : %d // ci : %d // si : %d\n",i,c[i],s[i]);*/
    }
}

void copie2(int* c,int* s1,int* s2,int tc,int ts1,int ts2){
    if(ts1+ts2>tc){
        printf("Le tableau cible est trop petit");return;
    }
    copie(s1,c,ts1,tc);
    copie(s2,&c[ts1],ts2,tc-ts1);
    printf("Les 2 tablo ont étés collés dans le 3eme\n");
}

void cs(char* s){
    int cmp=0;
    while(s[cmp]!='\0'){
        cmp++;
    }
    printf("%d\n",cmp);
}

void plp(char* s, char* s2){
    int cmp=0;
    while(s[cmp]!='\0'&&s2[cmp]!='\0'){
        if(s[cmp]!=s2[cmp]){printf(" \n");return;}
        printf("%c",s[cmp]);
        cmp++;
    }
    
}

void pti(int* t,int s){
    int i;
    for(i=0;i<s;i++){
        printf("%d : %d\n",i,t[i]);
    }
}



int main(int argc, char const *argv[])
{
    int t1[3];int t2[4];int tc[10];
    int i;
    if (argc!=3){
        printf("argv[1] et argv[2] seront les str utilisées pour les 2 derniers exos");
    }
    for(i=0;i<10;i++){
        *(tc+i)=0;
    }
    for (i = 0; i < 3; i++)
    {
        printf("Tab1 case %d ? ",i);
        scanf("%d",t1+i);
    }
    for (i = 0; i < 4; i++)
    {
        printf("Tab2 case %d ? ",i);
        scanf("%d",t2+i);
    }
    max(t1,3);
    copie(t2,t1,4,3);
    copie2(tc,t1,t2,10,3,4);
    cs(argv[1]);
    plp(argv[1],argv[2]);
    return 0;
}
