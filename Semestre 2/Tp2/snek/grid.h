#define NBL 22
#define NBC 36
#define X 640
#define Y 480
#define T 1000
#define DFREC MLV_draw_filled_rectangle
#define DREC MLV_draw_rectangle

#include <MLV/MLV_all.h>
#include <stdio.h>

typedef enum tiles {WALL='w',EMPTY=' ',FRUIT='f'}tiles;
extern char grid[NBL][NBC];
void debug(char grille[NBL][NBC]);
int compute_size(/*int w, int h*/);
void draw_grid(char grille[NBL][NBC], int a);