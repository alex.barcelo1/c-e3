#include "grid.h"


/*char grid[NBL][NBC] = {
    "wwwwww",
    "w  f w",
    "w    w",
    "w    w",
    "wf   w",
    "w  ffw",
    "w    w",
    "wwwwww"
};*/

    char grid[NBL][NBC] = {
        "w                                  w",
        "                                    ",
        "               f                    ",
        "                                    ",
        "     f               f              ",
        "                                    ",
        "                                    ",
        "               f                    ",
        "                                    ",
        "                                    ",
        "         wwwwwwwwww                 ",
        "                                    ",
        "                                    ",
        "                                    ",
        "                                    ",
        "                                    ",
        "                  f                 ",
        "                                    ",
        "         f                f         ",
        "                                    ",
        "                 f                  ",
        "w                                  w"
    };

void debug(char m[NBL][NBC]){
	int i,j;
	for (i = 0; i < NBL; i++)
	{
		for (j = 0; j < NBC; j++)
		{
			printf("%c",m[i][j]);
			if(j==NBC-1){
				printf("\n");
			}
		}
		
	}
	
}

int compute_size(/*int w, int h*/){
    /*int m1 = w/L;
    int m2 = h/(C-1);*/
    int m1 = X/NBL;
    int m2 = Y/(NBC-1);
    if (m1<m2){
        return m1;
    }else{
        return m2;
    }
}

void draw_grid(char grille[NBL][NBC], int a){
    MLV_Keyboard_button touche;

    int i,j;
    
    DFREC(0,0,X,Y,MLV_COLOR_WHITE);
    
    for(i=0;i<NBL;i++){
        for(j=0;j<NBC;j++){
            MLV_Color col = MLV_COLOR_BLACK;
            switch (grille[i][j])
            {
                case WALL:
                    col = MLV_COLOR_BROWN4;
                    break;
                case FRUIT:
                    col = MLV_COLOR_RED1;
                    break;
                case EMPTY:
                    col = MLV_COLOR_GREEN1;
                default:
                    break;
            }
            DFREC(i*a,j*a,a,a,col);
        }
    }
    
    MLV_actualise_window();
    while (1)
    {
        MLV_wait_keyboard(&touche, NULL, NULL);
        if (touche == MLV_KEYBOARD_ESCAPE){
            break;
        }
        
    }
}