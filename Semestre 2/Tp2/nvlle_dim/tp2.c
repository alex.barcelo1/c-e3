#include <stdio.h>

#define NBL 3
#define NBC 3

void pm(int m[NBL][NBC],int l,int c){
	int i,j;
	for (i = 0; i < l; i++)
	{
		for (j = 0; j < c; j++)
		{
			printf("matrice[%d][%d] = %d -- ",i,j,m[i][j]);
			if(j==c-1){
				printf("\n");
			}
		}
		
	}
	
}

void pgrille(char* grille, int nc, int t){
	int i;
	int sc=t/nc; /*size colonne = taille/nombre colonne*/
	for (i=0;i<t;i++){
		if(i%sc==0){
			printf("\nColonne %d : ",i/sc);
		}
		printf("%c",*(grille+i));
		
	}
	printf("\n");
	
}

int prodsomme(int m[NBL][NBC],int l,int c){
	int i,j;
	int s=0;
	int r=1;
	for (i = 0; i < l; i++)
	{
		for (j = 0; j < c; j++)
		{
			s+=m[i][j];
			if(j==c-1){
				r=r*s;
				printf("Somme ligne %d = %d\n",i,s);
				s=0;
			}
		}
		
	}	
	printf("Produit des sommes des lignes : %d\n",r);
	return r;
}



int main(){
	int matrice[NBL][NBC];
    int i,j;
    char grid[8][7] = {
        "bwrwbw",
        "wrwbwr",
        "bwrwbw",
        "wrwbwr",
        "bwrwbw",
        "wrwbwr",
        "bwrwbw",
        "wrwbwr"
    };
	/for (i = 0; i < NBL; i++)
	{
		for (j = 0; j < NBC; j++)
		{
			printf("matrice[%d][%d] = ",i,j);
			scanf("%d",&matrice[i][j]);
		}
		
	}
	pm(matrice,NBL,NBC);
	prodsomme(matrice,NBL,NBC);
    return 0;
}
