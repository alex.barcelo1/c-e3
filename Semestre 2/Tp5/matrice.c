#include <stdio.h>
#include <stdlib.h>

void pt(int* tab,int t){
    int i;
    for(i=0;i<t;i++){
        printf("%d-",*(tab+i));
    }
    printf("\n");
}

int ** alloue(int n, int m){
    int ** mr = malloc(n*sizeof(int*));
    int i/*,j*/;
    for (i=0; i<n ; i++){
        mr[i]=malloc(m*sizeof(int));
        /*for(j=0;j<m;j++){
            mr[i][j]=i+j;
        }*/
    }
    
    return mr;
}

void libere(int ** mat,int n, int m){
    int i;
    for(i=0 ; i<n ; i++){
        free(mat[i]);
    }
    free(mat);
}

int main(int argc, char const *argv[])
{
    int ** m=alloue(10,10);
    for(int i=0;i<10;i++){
        pt(m[i],10);
    }
    libere(m,10,10);
    return 0;
/*
==5664== 
0-1-2-3-4-5-6-7-8-9-
1-2-3-4-5-6-7-8-9-10-
2-3-4-5-6-7-8-9-10-11-
3-4-5-6-7-8-9-10-11-12-
4-5-6-7-8-9-10-11-12-13-
5-6-7-8-9-10-11-12-13-14-
6-7-8-9-10-11-12-13-14-15-
7-8-9-10-11-12-13-14-15-16-
8-9-10-11-12-13-14-15-16-17-
9-10-11-12-13-14-15-16-17-18-
==5664== 
==5664== HEAP SUMMARY:
==5664==     in use at exit: 0 bytes in 0 blocks
==5664==   total heap usage: 12 allocs, 12 frees, 1,504 bytes allocated
*/
}
