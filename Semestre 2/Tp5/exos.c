#include <stdlib.h>
#include <stdio.h>

float* create(int t){
    float* r = malloc(sizeof(int)*t);
    return r; 
}

float* ccreate(int t){
    float* r = calloc(sizeof(float),t);
    return r; 
}

float pt(float* tab,int t){
    int i;
    for(i=0;i<t;i++){
        printf("%f-",*(tab+i));
    }
    printf("\n");
}

char* concat(char* s1,char* s2){
    int i=0,s=0,ss1=0;
    for(i=0;(*(s1+i) != '\0' || *(s2+i) != '\0'); i++){
        s++;
    }
    char * sr = malloc(s*sizeof(char));
    for(i=0;*(s1+i)!='\0';i++){
        *(sr+i) = *(s1+i);
        ss1++;
    }
    for(i=ss1;*(s2+i-ss1)!='\0';i++){
        *(sr+i) = *(s2+i-ss1);
    }
    return sr;
}

int main(int argc, char const *argv[])
{
    float *tab = create(30);
    pt(tab,30);
    printf("malloc en haut calloc en bas normalement malloc affiche des valeurs en mémoire random et calloc alloue la tableau avec des 0\n");
    float *ctab = ccreate(30);
    pt(tab,30);
    char * sf = "==4585== LEAK SUMMARY:\n==4585==    definitely lost: 240 bytes in 2 blocks\n==4585==    indirectly lost: 0 bytes in 0 blocks\n==4585==      possibly lost: 0 bytes in 0 blocks\n==4585==    still reachable: 0 bytes in 0 blocks\n==4585==         suppressed: 0 bytes in 0 blocks";
    char * s = "==4614== HEAP SUMMARY:\n==4614==     in use at exit: 0 bytes in 0 blocks\n==4614==   total heap usage: 3 allocs, 3 frees, 1,264 bytes allocated\n==4614== \n==4614== All heap blocks were freed -- no leaks are possible";
    free(tab);
    free(ctab);
    printf("AVEC FREE : %s \n On perd definitivement 240 (60*8) bytes\n\nSANS FREE : %s\n",s,sf);   
    char * s1 = "bon";
    char * s2 = "jour";
    s1 = concat(s1,s2);
    printf("%s\n",s1);
    free(s1);
    return 0;
}
