#define _GNU_SOURCE
#include "grid.h"
#define FPS 300 /*PERIODE de rafraichissement*/
#define WIN_SCORE 1

const char* program_name;
int vdebug = 0; 
char vimprim[] = "";

void print_help(){
    printf("Jeu Snake E3 C\n-h --help : pour ce message\n");
    printf("-i --ingrid PATH_TO_FILE : pour jouer une grille perso\n");
    printf("-d --debug : pour print des infos relatives à la partie en cours\n/!\\Beaucoup d'infos vont être print, il est conseillé de lancer le jeu avec ./game.out -d > log\n");
}

void stripcp(char* source, char * cible){
    int i;
    for(i=0;source[i] != '\0' && source[i] != '\n';i++){
        cible[i]=source[i];
    }
}

int compte_fruit(Grid* g){
    int i,j;
    int c=0;
    for (i=0; i<g->snbl; i++){
        for(j=0; j<g->snbc; j++){
            if (g->sgrid[i][j] == FRUIT){
                if(vdebug){printf("Fruit en %d-%d\n",i,j);}
                c++;
            }
        }
    }
    return c;
}

int compte_ligne(FILE* f){
    int ret=0;
    char ch;
    while(!feof(f)){
        ch = fgetc(f);
        if(ch == '\n'){
            ret++;
        }
    }
    return ret;/*la dernière ligne n'a pas de \n */
}

void gg(){
    int xsmile[4] = {X/7+X/30,X/7+X/100,X/7-X/100,X/7-X/30};
    int ysmile[4] = {Y/7+Y/30,Y/7+2*Y/30,Y/7+2*Y/30,Y/7+Y/30};
    int ysmile2[4] = {Y/7+Y/30,Y/7+3*Y/30,Y/7+3*Y/30,Y/7+Y/30};
  
    MLV_draw_filled_circle(X/7,Y/7,X/10,MLV_COLOR_YELLOW1);
    MLV_draw_filled_circle(X/7+X/30,Y/7-Y/30,X/50,MLV_COLOR_RED);
    MLV_draw_filled_circle(X/7-X/30,Y/7-Y/30,X/50,MLV_COLOR_RED);
    MLV_draw_bezier_curve(xsmile,ysmile,4,MLV_COLOR_BLACK);
    MLV_draw_bezier_curve(xsmile,ysmile2,4,MLV_COLOR_BLACK);
}

void lose(int e){
    MLV_Keyboard_button touche;
    char* lmess ;
    if (e == 'w'){lmess = "Tu as mangé un mur - esc pour quitter";}
    if (e == 's'){lmess = "Tu as mangé ta queue - esc pour quitter";}
    if (e == 'f'){lmess = "Félicitations voici ton invitation pour rejoindre le FaZe Clan";gg();}
    MLV_draw_text(X/10,Y/2,lmess,MLV_COLOR_BLACK);
    MLV_actualise_window();
    while(1){
        MLV_get_event(&touche,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
        if (touche == MLV_KEYBOARD_ESCAPE){
            exit(0);
        }
    }
}

void place_snake(Grid* g, Snake s){
    int i;
    int sx,sy;
    for(i=0;i<SNAKE_SIZE;i++){
        sx = s.body[i].x;
        sy = s.body[i].y;
        g->sgrid[sx][sy] = SNAKE;
    }
}

int main(int argc, char* argv[])
{
    MLV_Keyboard_button touche;
    int cloop = 0;
    int a;
    int win;
    int i;
    int ig;
    int nblf,nbcf;
    int perdu=0;
    int next_option;
    char* buf=NULL;
    size_t size_buf, size_tmp;
    Snake s;
    tiles eat;
    Grid* vsgrid;
    /*DEBUT GESTION OPTIONS*/  
    const char* short_opt="hdi:";
    const struct option long_opt[] = {
        {"help",0,NULL,'h'},
        {"debug",0,NULL,'d'},
        {"ingrid", 1, NULL, 'i'},
        { NULL, 0, NULL, 0} 
    };
    program_name=argv[0];
    next_option=0;
    while (next_option != -1)
    {
        next_option=getopt_long(argc,argv,short_opt,long_opt,NULL);
        switch (next_option){
        case 'h':
            print_help();
            return 0;
        case 'd':
            vdebug = 1;
            break;
        case 'i':
            strcpy(vimprim,optarg);
            break;
        default:
            break;
        }
    }
    /*FIN GESTION OPTIONS*/

    /*CHOIX GRILLE*/

    if (strcmp(vimprim,"") == 0){printf("Aucun level passé utilisation de levels/default \n");strcpy(vimprim,"levels/default");}

    FILE* fg = fopen(vimprim,"r");
    if(fg == NULL){
        printf("File not found, utilisation de la grille par défaut \n");
        fg = fopen("levels/default","r");
    }
    nblf = compte_ligne(fg);
    rewind(fg);
    nbcf = getline(&buf,&size_buf,fg);
    if(nbcf==-1){
        printf("erreur dans le fichier : %s",vimprim);
        exit(1);
    }
    else nbcf--;
    vsgrid = allocate_grid(nblf,nbcf);
    stripcp(buf,vsgrid->sgrid[0]);
    /*buf = malloc(nbcf+1);*/
    /*char buf[vsgrid->snbc+1];*/
    for (ig = 1; ig < nblf; ig++){
        size_tmp = getline(&buf,&size_buf,fg);        
        if(size_tmp != nbcf+1){
            printf("Erreur : La ligne %d à %ld chars alors que la 1ère en à %d",ig+1,size_tmp,nbcf-1);
            exit(1);
        }
        stripcp(buf,vsgrid->sgrid[ig]);
    }
    if (vdebug){printf("Dimension de la grille lue (%s) %d - %d\n",vimprim,nblf,nbcf);}
    free(buf);
    fclose(fg);
        
    
    /*FIN CHOIX GRILLE*/

    win=compte_fruit(vsgrid)/*WIN_SCORE*/;

    /*Snake s = {{1,3},{1,2},{1,1},{1,0}};*/
    int posS[8] = {1,3,1,2,1,1,1,0};
    for(i=0;i<SNAKE_SIZE;i++){
        s.body[i].x=posS[i*2];
        s.body[i].y=posS[i*2+1];
    }
    s.next_dir=TOP;
    place_snake(vsgrid,s);
    /*FIN LOGIQUE SNAKE*/

    MLV_create_window("Fenetre dessin","Icone dessin",X,Y);
    
    a = compute_size(vsgrid->snbl,vsgrid->snbc);
    while(1){
        
            
        MLV_get_event(&touche,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
        if (touche == MLV_KEYBOARD_ESCAPE){
            break;
        }else if(!perdu){
            set_dir(touche,&s);
        }
        if (cloop%FPS == 0){
            if (vdebug) {printf ("grille : \n");debug(vsgrid);}
            eat = move_snake(&s,vsgrid);
            if (eat == FRUIT){win--;}
        }
        if((eat!=EMPTY && eat!=FRUIT) || (eat==FRUIT && win==0)){
            free_grid(vsgrid);
            lose(eat);
        }
        place_snake(vsgrid,s);
        draw_grid(vsgrid,a);
        MLV_actualise_window();       
        
        cloop = (cloop+1)%FPS;
    
    }
    free_grid(vsgrid);
/*  
    while (1)
    {
        MLV_wait_keyboard(&touche, NULL, NULL);
        if (touche == MLV_KEYBOARD_ESCAPE){
            break;
        }
        
    }*/

    return 0;
}
