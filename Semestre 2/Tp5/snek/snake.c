#include "snake.h"

void show_snake(Snake s){
    int i;
    printf("next_dir:%d\n",s.next_dir);
    for(i=0;i<SNAKE_SIZE;i++){
        printf("s[%d].x:%d - .y:%d\n",i,s.body[i].x,s.body[i].y);
    }
    printf("\n");
}

void set_dir(MLV_Keyboard_button t,Snake* s){
    /*La grille est affiché tournée de 90°*/
    switch (t){
    case MLV_KEYBOARD_UP:
        s->next_dir = RIGHT;
        break;
    case MLV_KEYBOARD_DOWN:
        s->next_dir = LEFT;
        break;
    case MLV_KEYBOARD_LEFT:
        s->next_dir = BOTTOM;
        break;
    case MLV_KEYBOARD_RIGHT:
        s->next_dir = TOP;
        break;
    default:
        break;
    }
}

void crawl(Snake* s,int nbl,int nbc){
    int dx=0;
    int dy=0;
    int i;
    int nhx,nhy;
    switch (s->next_dir){
        
        case TOP:
            dx=1;
            break;
        case BOTTOM:
            dx=-1;
            break;
        case LEFT:
            dy=1;
            break;
        case RIGHT:
            dy=-1;
            break;
        default:
            break;
    }
    if (vdebug) show_snake(*s);
    for (i=SNAKE_SIZE-1; i>0; i--){/*-1 pour eviter d'overflow sur nextdir*/
        /*printf("i:%d -- nd:%d \n",i,s->next_dir);*/
        s->body[i].x = s->body[i-1].x;
        s->body[i].y = s->body[i-1].y;
    }  
    nhx = ((s->body[0].x+dx+nbl)%nbl);
    nhy = ((s->body[0].y+dy+nbc)%nbc);
    /*comme move snake ne modifie pas la grille on passe la position de sa nouvelle tête qu'on évaluera AVANT de bouger le serpent sur la grille*/
    /**hx=nhx;
    *hy=nhy;*/
    s->body[0].x = nhx;/*+NBL pour les depassements négatifs, segfault en <drawgrid+80>*/
    s->body[0].y = nhy;
    if (vdebug) show_snake(*s);
}