#include <stdlib.h>
#include <stdio.h>

void f(int* tab){
    printf("Taille du POINTEUR tab = %ld\n",sizeof(tab));
}

int main(int argc, char const *argv[])
{
    int tab[10];
    f(tab);
    return 0;
}
