#include <MLV/MLV_all.h> /*nécessaire à cause des fonctions qui utilisent le clavier et les couleurs*/


#define X 640
#define Y 480
#define T 1000
#define FR MLV_draw_filled_rectangle

int main()
{
    int xtoit[3] = {X/4,3*X/4+2*X/8,(X/4+3*X/4+2*X/8)/2};
    int ytoit[3] = {2*Y/5,2*Y/5,Y/8};
    int xsmile[4] = {X/7+X/30,X/7+X/100,X/7-X/100,X/7-X/30};
    int ysmile[4] = {Y/7+Y/30,Y/7+2*Y/30,Y/7+2*Y/30,Y/7+Y/30};
    int ysmile2[4] = {Y/7+Y/30,Y/7+3*Y/30,Y/7+3*Y/30,Y/7+Y/30};
    MLV_Keyboard_button touche;

    MLV_create_window("Fenetre dessin","Icone dessin",X,Y);
    
    /*Background*/
    FR(0,0,X,Y,MLV_COLOR_MEDIUM_BLUE);
    FR(0,Y,X,-Y/10,MLV_COLOR_GREEN4);
    /*Soleil*/
    MLV_draw_filled_circle(X/7,Y/7,X/10,MLV_COLOR_YELLOW1);
    MLV_draw_filled_circle(X/7+X/30,Y/7-Y/30,X/50,MLV_COLOR_BLACK);
    MLV_draw_filled_circle(X/7-X/30,Y/7-Y/30,X/50,MLV_COLOR_BLACK);
    MLV_draw_bezier_curve(xsmile,ysmile,4,MLV_COLOR_BLACK);
    MLV_draw_bezier_curve(xsmile,ysmile2,4,MLV_COLOR_BLACK);
    /*Maison*/
    FR(X/4+X/8,Y,X/2,-3*Y/5,MLV_COLOR_DARKGOLDENROD3);
    /*Porte*/
    FR(X/4+2*X/8,Y,X/8,-Y/4,MLV_COLOR_BROWN);
    /*Fenetre*/
    FR(7*X/10,2*Y/3,X/8,-Y/7,MLV_COLOR_ROYALBLUE);
    /*Toit*/
    MLV_draw_filled_polygon(xtoit,ytoit,3,MLV_COLOR_ROSYBROWN);
    MLV_draw_text(X/2+X/18,2*Y/6-Y/20,"q pour quitter",MLV_COLOR_BLACK);

    MLV_actualise_window();
    
    while (1)
    {
        MLV_wait_keyboard(&touche, NULL, NULL);
        if (touche == MLV_KEYBOARD_q){
            break;
        }
        
    }
    

    return 0;
    

}
