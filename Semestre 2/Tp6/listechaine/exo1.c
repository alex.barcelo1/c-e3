#include <stdio.h>
#include <stdlib.h>

typedef struct _cell{
    int v;
    struct _cell* next;
}Cell;

typedef enum bool{true=1,false=0} bool;

Cell* emptylist(){
    Cell* lr = NULL;
    lr = calloc(1,sizeof(lr));
    lr->next = NULL;
    lr->v = 999;
    return lr;
}

bool isempty(Cell* l){
    Cell* emptylis = emptylist();
    if(l->next==emptylis->next && l->v==emptylis->v){
        free(emptylis);
        return true;
    }
    free(emptylis);
    return false;
}

void add_first(Cell** cell, int v){
    Cell* rcell = malloc(sizeof(Cell));
    rcell->v = v;
    rcell->next = *cell;
    (*cell) =rcell;
}

void remove_first(Cell ** cell){
    Cell * tmp = (*cell)->next;
    free(*cell);
    (*cell) = tmp;  
}

Cell* get(int v,Cell** l){/*PAS NECESSAIRE POUR PROJEY EY BEAUCOUP TROP RESTRICTIF*/
    Cell ** tmp = l;
    Cell ** tmpr;
    Cell ** maillonr;
    int skipnext=0;
    int nodouble=0;
    int skipval=0;
    int nv,nn;
    while( !isempty(*l) ){

        if((*tmp)->next->v == v && nodouble==0){/*si le maillon suivant est v*/
            skipnext=1;
            nodouble=1;
        }
        
        if(!skipnext){        
            (*tmpr)->next=(*tmp)->next;
        }
        
        if(!skipval){
            (*tmpr)->v=(*tmp)->v;
            skipval=0;
        }

        if(skipnext){
            skipnext=0;
            skipval=1;
        }
        
        (*tmp)=(*tmp)->next;
    }
    *l = *tmpr;
}

Cell* create_list(int s){
    int i;
    Cell* h = malloc(sizeof(Cell));
    h->next = emptylist();
    h->v=0;
    for(i=1;i<s;i++){
        add_first(&h,i);
    }
    return h;
}

void disp_list(Cell* l){ /*recursif*/
    if(!isempty(l)){
        printf("val : %d\n",l->v);
        disp_list(l->next);
    }
}

void printList(Cell *node)/*iteratif*/
{ 
    Cell* el=emptylist();
    while (node->next != el-> next) 
    { 
    printf(" %d ", node->v); 
    node = node->next; 
    } 
    free(el);
} 

void freel(Cell* l){
    Cell* el=emptylist();
    Cell* tmp;
    while(!isempty(l)){
        tmp=l->next;
        printf("valeur free : %d\n",l->v);
        free(l);
        l=tmp;
    }
    free(l);
    free(el);
}

int main(int argc, char const *argv[])
{ 
    Cell* list = create_list(10);
    disp_list(list);
    remove_first(&list);
    disp_list(list);
    freel(list);
    return 0;
}
