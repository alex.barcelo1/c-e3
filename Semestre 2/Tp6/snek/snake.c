#include "snake.h"

lSnake* new_snake(){
    lSnake* rsnake;
    rsnake = malloc(sizeof(lSnake));
    /*rsnake->seg_list = malloc(sizeof(lCoord));*/
    rsnake->seg_list = NULL;
    rsnake->size = 0;
    rsnake->next_dir = TOP;
    return rsnake;
}

void free_snake(lSnake* ls){
    printf("Le serpent est en train d'être free, il ne sera donc plus utilisable ensuite\n");
    lCoord* lc_to_free;
    while (ls->seg_list != NULL){
        lc_to_free = ls->seg_list;
        ls->seg_list = ls->seg_list->next;
        free(lc_to_free);
    }
    free(ls);
}

void print_snake(lSnake* ls){
    lCoord* tmp = ls->seg_list;
    printf("next_dir : %d\n",ls->next_dir);
    while (tmp != NULL){
        printf("Valeur x,y : %d,%d\n",tmp->x,tmp->y);
        tmp = tmp->next;
    }
}

void add_fin(lSnake * ls, int x,int y){
    lCoord* newseg = malloc(sizeof(lCoord));
    newseg->x = x;
    newseg->y = y;
    newseg->next = NULL;
    lCoord * actuel = ls->seg_list;
    lCoord * precedent = NULL;

    if(ls->seg_list == NULL){
        ls->seg_list = newseg;
        return; 
    }

    while(actuel != NULL){/*Si on est pas à la fin on avance jusqu'à la fin*/
        precedent = actuel;
        actuel = actuel->next;
    }
    /*Si x->y->NULL*/
    precedent->next = newseg;/*Alors x->y->newseg->NULL*/
    /*free(actuel); pas besoin comme actuel est null*/
}

/*void show_snake(Snake s){
    int i;
    printf("next_dir:%d\n",s.next_dir);
    for(i=0;i<SNAKE_SIZE;i++){
        printf("s[%d].x:%d - .y:%d\n",i,s.body[i].x,s.body[i].y);
    }
    printf("\n");
}*/

void set_dir(MLV_Keyboard_button t,lSnake* ls){
    /*La grille est affiché tournée de 90°*/
    switch (t){
    case MLV_KEYBOARD_UP:
        ls->next_dir = RIGHT;
        break;
    case MLV_KEYBOARD_DOWN:
        ls->next_dir = LEFT;
        break;
    case MLV_KEYBOARD_LEFT:
        ls->next_dir = BOTTOM;
        break;
    case MLV_KEYBOARD_RIGHT:
        ls->next_dir = TOP;
        break;
    default:
        break;
    }
}

void crawl(lSnake* ls,int nbl,int nbc){
    int dx=0;
    int dy=0;
    int nhx,nhy;
    lCoord* current = ls->seg_list;
    lCoord* precedent = NULL;
    int i;
    switch (ls-> next_dir){        
        case TOP:
            dx=1;
            break;
        case BOTTOM:
            dx=-1;
            break;
        case LEFT:
            dy=1;
            break;
        case RIGHT:
            dy=-1;
            break;
        default:
            break;
    }
    if (vdebug) print_snake(ls);
    
    /*for (i=SNAKE_SIZE-1; i>0; i--){-1 pour eviter d'overflow sur nextdir*/
        /*printf("i:%d -- nd:%d \n",i,s->next_dir);
        s->body[i].x = s->body[i-1].x;
        s->body[i].y = s->body[i-1].y;
    }*/     
    int t = ls->size; 
/*printf("%d\n",t);*/
    for(i=1;i<t;i++){
        precedent = current;
        current = current->next;
        precedent->x = current->x;
        precedent->y = current->y;
    }

    nhx = (precedent->x+dx+nbl)%nbl;
    nhy = (precedent->y+dy+nbc)%nbc;
    /*nhx = ((s->body[0].x+dx+nbl)%nbl);
    nhy = ((s->body[0].y+dy+nbc)%nbc);*/
    /*comme move snake ne modifie pas la grille on passe la position de sa nouvelle tête qu'on évaluera AVANT de bouger le serpent sur la grille*/
    /**hx=nhx;
    *hy=nhy;*/
    precedent->next->x = nhx;/*+NBL pour les depassements négatifs, segfault en <drawgrid+80>*/
    precedent->next->y = nhy;
    if (vdebug) print_snake(ls);
}