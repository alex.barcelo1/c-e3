#include "grid.h"

/*char grid[NBL][NBC] = {
    "w                                  w",
    "                                    ",
    "               f                    ",
    "                                    ",
    "     f               f              ",
    "                                    ",
    "                                    ",
    "               f                    ",
    "                                    ",
    "                                    ",
    "         wwwwwwwwww                 ",
    "                                    ",
    "                                    ",
    "                                    ",
    "                                    ",
    "                                    ",
    "                  f                 ",
    "                                    ",
    "         f                f         ",
    "                                    ",
    "                 f                  ",
    "w                                  w"
};*/

void debug(Grid* g){
	int i,j;
	for (i = 0; i < g->snbl; i++)
	{
        printf("[");
		for (j = 0; j < g->snbc; j++)
		{
			printf("%c",g->sgrid[i][j]);
			if(j==g->snbc-1){
				printf("]");
                printf("\n");
			}
		}
		
	}
	
}

Grid* allocate_grid(int n,int m){
    Grid* g = malloc(sizeof(Grid));
    int i;
    g->sgrid = malloc(n*sizeof(char*));
    for(i=0;i<n;i++){
        g->sgrid[i] = malloc(m*sizeof(char));
    }
    g->snbl = n;
    g->snbc = m; 
    return g;
}

void free_grid(Grid * g){
    int i;
    for(i=0;i<(g->snbl);i++){
        free(g->sgrid[i]);
    }
    free(g->sgrid);
    free(g);
    printf("La grille est en train d'être free, elle ne sera donc plus utilisable ensuite\n");
}

int compute_size(int nbl, int nbc){
    /*int m1 = w/L;
    int m2 = h/(C-1);*/
    int m1 = X/nbl;
    int m2 = Y/nbc;
    if (m1<m2){
        return m1;
    }else{
        return m2;
    }
}

/*void log(char* fname, char g[NBL][NBC]){
    FILE* f = fopen(fname,"a");
    char buf[NBC];
    while(fgets(buf,NBC,))
}*/

tiles move_snake(lSnake* ls, Grid* g){
    /*int tailx = s->body[SNAKE_SIZE-1].x;
    int taily = s->body[SNAKE_SIZE-1].y;*/
    lCoord* current = ls->seg_list;
    lCoord* precedent = NULL;
    int tailx = ls->seg_list->x;
    int taily = ls->seg_list->y;
    int hx,hy;
    g->sgrid[tailx][taily] = EMPTY;
    crawl(ls,g->snbl,g->snbc);
    
    while(current != NULL){
        precedent = current;
        current = current->next;
    }
    hx = precedent->x;
    hy = precedent->y;
    /*hx=s->body[0].x;
    hy=s->body[0].y;*/
    return g->sgrid[hx][hy];
}


void draw_grid(Grid* g, int a){ /*a=compute_size*/
    
    int i,j;
    DFREC(0,0,X,Y,MLV_COLOR_WHITE);
    
    /*if (strcmp(vimprim,NULL)){log(vimprim,grille);}*/
    for(i=0;i<g->snbl;i++){
        for(j=0;j<g->snbc;j++){
            MLV_Color col;
            switch (g->sgrid[i][j])
            {
                case WALL:
                    col = MLV_COLOR_BROWN4;
                    break;
                case FRUIT:
                    col = MLV_COLOR_RED1;
                    break;
                case EMPTY:
                    col = MLV_COLOR_GREEN1;
                    break;
                case SNAKE:
                    col = MLV_COLOR_YELLOW1;
                    break;
                default:
                    printf("Unexpected char while parssing : %c\n",g->sgrid[i][j]);
                    exit(1);
            }
            DFREC(i*a,j*a,a,a,col);
        }
    }
    

}