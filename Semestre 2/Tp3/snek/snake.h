#ifndef fSNAKE
#define fSNAKE

/*Tous les define/include sont ici pour eviter les dependances cycliques et garder show_snake dans snake.c (à cause de NBC et NBL)*/
#define NBL 22
#define NBC 36
#define X 640
#define Y 480
/*#define T 1000*/
#define DFREC MLV_draw_filled_rectangle
#define DREC MLV_draw_rectangle

#include <MLV/MLV_all.h>
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdio.h>
#define SNAKE_SIZE 4


extern int vdebug ;
typedef enum dir {TOP,BOTTOM,LEFT,RIGHT}dir;

typedef struct _coord
{
    int x;
    int y;
}Coord;

typedef struct _snake
{
    Coord body[SNAKE_SIZE];
    dir next_dir;
}Snake;

void crawl(Snake* s);
void set_dir(MLV_Keyboard_button t,Snake* s);
#endif