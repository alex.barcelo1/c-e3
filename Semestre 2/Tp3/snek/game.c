#include "grid.h"

const char* program_name;
int vdebug = 0; 

void print_help(){
    printf("Jeu Snake E3 C\n");
}

void place_snake(char g[NBL][NBC], Snake s){
    int i;
    int sx,sy;
    for(i=0;i<SNAKE_SIZE;i++){
        sx = s.body[i].x;
        sy = s.body[i].y;
        g[sx][sy] = SNAKE;
    }
}

int main(int argc, char* argv[])
{
    MLV_Keyboard_button touche;
    /*DEBUT GESTION OPTIONS*/  
    const char* short_opt="hd";
    const struct option long_opt[] = {
        {"help",0,NULL,'h'},
        {"debug",0,NULL,'d'},
        { NULL,       0, NULL, 0   } 
    };
    program_name=argv[0];
    int next_option=0;
    while (next_option != -1)
    {
        next_option=getopt_long(argc,argv,short_opt,long_opt,NULL);
        switch (next_option){
        case 'h':
            print_help();
            return 0;
        case 'd':
            vdebug = 1;
            break;
        default:
            break;
        }
    }
    /*FIN GESTION OPTIONS*/

    /*Snake s = {{1,3},{1,2},{1,1},{1,0}};*/
    Snake s;
    int i;
    int posS[8] = {1,3,1,2,1,1,1,0};
    for(i=0;i<SNAKE_SIZE;i++){
        s.body[i].x=posS[i*2];
        s.body[i].y=posS[i*2+1];
    }
    place_snake(grid,s);
    /*FIN LOGIQUE SNAKE*/

    MLV_create_window("Fenetre dessin","Icone dessin",X,Y);
    int a = compute_size(/*X,Y*/);
    while(1){
        MLV_wait_keyboard(&touche, NULL, NULL);
        if (touche == MLV_KEYBOARD_ESCAPE){
            break;
        }else{
            set_dir(touche,&s);
        }
        move_snake(&s,grid);
        place_snake(grid,s);
        draw_grid(grid,a);
        MLV_actualise_window();       
    }

/*  
    while (1)
    {
        MLV_wait_keyboard(&touche, NULL, NULL);
        if (touche == MLV_KEYBOARD_ESCAPE){
            break;
        }
        
    }*/

    return 0;
}
