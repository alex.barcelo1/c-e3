#ifndef fGRID
#define fGRID

#include "snake.h"

extern char grid[NBL][NBC];
typedef enum tiles {WALL='w',EMPTY=' ',FRUIT='f',SNAKE='s'}tiles;
extern char grid[NBL][NBC];
void debug(char grille[NBL][NBC]);
int compute_size(/*int w, int h*/);
void draw_grid(char grille[NBL][NBC], int a);
void move_snake(Snake* s, char grille[NBL][NBC]);

#endif