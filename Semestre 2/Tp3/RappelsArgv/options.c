#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>

const char* program_name;

int main(int argc, char *argv[])
{
    int next_option;
    const char* const short_options = "ho:v";
    const struct option long_options[] = {
        { "help",     0, NULL, 'h' },
        { "output",   1, NULL, 'o' },
        { "verbose", 0, NULL, 'v' },
        { NULL,       0, NULL, 0   }   /* Requis à la fin du tableau.  */
    };
    program_name = argv[0];
    while(next_option!=-1){
        next_option=getopt_long(argc,argv,short_options,long_options,NULL);
        switch (next_option)
        {
        case 'h':
            printf("HELP\n");
            break;
        case 'o':
            printf("OUTPUT %s\n",optarg);/*optagr est une variable globale de getopt.h*/
            break;
        case 'v':
            printf("VERBOSE\n");
            break;
        default:
            /*printf("Invalid option\n"); L'output option invalide est déjà pris en compte*/
            break;
        }
    }
    return 0;
}
