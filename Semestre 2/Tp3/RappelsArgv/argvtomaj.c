#include <stdio.h>

void strmaj(char* str){
    int i;
    for(i=0;*(str+i)!='\0';i++){
        if (*(str+i)>='a' && *(str+i)<='z'){
            *(str+i)=*(str+i)-32; /*En ASCII une lettre miniscule est à 32 (0x20) positions de la majuscule associée 'A'=41 'a'=67)*/
        }
    }
}

int main(int argc, char *argv[])
{
    if(argc == 1){return 1;}
    printf("Programme : %s",argv[0]);
    printf("argv[1] : %s\n",argv[1]);
    strmaj(argv[1]);
    printf("argv[1] en majuscule : %s\n",argv[1]);
    return 0;
}
