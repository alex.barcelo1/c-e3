#include <stdio.h>

int main(int argc, char *argv[])
{
    if(argc==1){return 1;}
    int i,s,p;
    s=0;
    for(i=1;i<argc;i++){
        printf("%s + ",argv[i]);
        sscanf(argv[i],"%d",&p);
        s+=p;
    }
    printf("0 = %d \n",s);
    return 0;
}
