#include "graphics.h"
#include "test.h"
#define col gr_set_color

//Doc : http://para.inria.fr/~peskine/enseignement/iup-2002/graphics-doc.html

void draw(char* s){

	gr_open_graph(s);
	int X = gr_size_x();
	int Y=gr_size_y();
	
	/*background*/
	col(blue);
	gr_fill_rect(0,0,X,Y);
	
	/*toit*/
	col(red);
	int triangle[] = {X/2,Y,0,Y/3*2,X,Y/3*2};
	gr_fill_poly(triangle,3);
	
	/*maison*/
	col(white);
	gr_fill_rect(X/10,0,8*X/10,2*Y/3);
	
	/*fenetre*/
	col(RGB(220,220,256));
	gr_fill_rect(2*X/3,Y/2,X/7,Y/7);
	
	/*porte*/
	col(RGB(88,41,0));
	gr_fill_rect(4*X/10,0,2*X/10,Y/3);	
	
		
	(void) gr_wait_event(KEY_PRESSED);
	gr_close_graph();

}


int main(){
	test(1);
	draw("");
	return 0;
}
