#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#define TABLEAUT monTableau[5] = {1,2,3,4,5}; int t=5;
#define TABLEAUIT monTableau[5] = {5,4,3,2,1}; int t=5;
#define TABLEAUA monTableau[5] = {3,2,5,4,1}; int t=5;
#define BTABLEAUA monTableau[10] = {3,10,2,8,5,9,4,6,7,1}; int t=10;
#define TABLEAUMOM montableau[11] = {2,6,18,17,18,3,-7,18,-7,6,2}; int t=11;
#define TABLEAUMOMA montableau[15] = {2,6,18,17,18,3,-7,18,-7,6,2,10000,-100000,1,1}; int t=15;


void pt(int* tab,int t){
    int i;
    for(i=0;i<t;i++){
        printf("%d-",*(tab+i));
    }
    printf("\n");
}

void ps(int* tab){
    int i=0;
    while(*(tab+i)!='\0'){
        printf("%c",*(tab+i));
        i++;
    }
    printf("\n");
}

void swap(int* a,int* b){
    int x = *a;
    *a=*b;
    *b=x;
}

int max_value(int* tab, int t){
    int i;
    int max = *tab;
    for(i=1;i<t;i++){
        if(max<*(tab+i)){
            max=*(tab+i);
        }
    }
    return max;
}

int count(int* tab, int v, int t){
    int i;
    int cmp=0;
    for(i=0;i<t;i++){
        if(*(tab+i)==v){
            cmp++;
        }
    }
    return cmp;
}

int countchar(char* tab, char v){
    int i=0;
    int cmp=0;
    while((*(tab+i))!='\0'){
        if(*(tab+i)==v){
            cmp++;
        }
        i++;
    }
    return cmp;
}