/*gcc -ansi -pedantic -Wall exo.c*/
#include <stdio.h>

int add(int a, int b){
	return a+b;
}

void add_and_print(int a,int b){
	printf("add : %d\n",add(a,b));
}

int fact(int n){
	if (n==1){return 1;}
	return fact(n-1)*n;
}

int facti(int n){
	int r=1;
	int i;
	for(i=n;i>0;i--){
		r=r*i;
	}
	return r;
}

int pui(int a,int b){
	if(b==0){
		return 1;
	}

	if(b==1){
		return a;
	}

	return a*pui(a,b-1);

}

int puii(int a, int b){
	int r=1;
	int i;
	for(i=b;i>0;i--){
		r=r*a;
	}
	return r;
}

void print_n_char(char c, unsigned int a){
	if(a==0) { return;}
	printf("%c",c);
	print_n_char(c,a-1);	
}

void pn(char c, unsigned int a){print_n_char(c,a);}

void print_n_char_i(char c, unsigned int a){
	int i;	
	for(i=a;i>0;i--){
		printf("%c",c);	
	}
	printf("\n");
}

void pyrarec(int c,int t){
	int l=t*2;/* largeur = taille totale*2 */	
	if(c==-1){return ;}
	pyrarec(c-1,t);
	print_n_char(' ',l/2-c); /*moitié largeur max-nb d'étoiles*/
	print_n_char('*',c*2+1); /*+1 pour que le truc soit plus joli*/
	printf("\n");
	
}

void pyrai(int a){
	int i;
	for (i=0;i<=a;i++){
		pn(' ',a-i);
		pn('*',i*2+1);
		printf("\n");	
	}
}

void pyra(int a){pyrarec(a,a);}

int main(){
	add_and_print(14,7);
	printf("fact : %d\n",fact(4));
	printf("pui : %d\n",pui(2,3));
	printf("fact : %d\n",facti(4));
	printf("pui : %d\n",puii(5,3));
	/*print_n_char_i('w',5);
	print_n_char('w',5);	
	*/
	pyra(2);
	pyrai(10);
	return 0;
}
