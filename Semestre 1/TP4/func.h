#ifndef MY_HEADER
#define MY_HEADER

typedef struct complex
{ 
    int a;
    int b;
}complex;

complex new_complex(int a,int b);
void print_complex(complex c);
complex sum(complex c1, complex c2);
complex myconj(complex c);
int norm(complex c); 

#endif
