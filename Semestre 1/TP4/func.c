#include "func.h"
#include <math.h>
#include <stdio.h>


complex new_complex(int a,int b)
{
    complex ret = {a,b};
    return ret;
}

void print_complex(complex c){
    if (c.b>=0){
        printf("%d+%di\n",c.a,c.b);
    }else
    {
        printf("%d%di\n",c.a,c.b);
    } 
}

complex sum(complex c1,complex c2){
    complex c = {c1.a+c2.a,c1.b+c2.b};
    return c;
}

complex myconj(complex c){
    complex cr={c.a,-c.b};
    return cr;
}

int norm(complex c){
    return sqrt(c.a*c.a+c.b*c.b);
}