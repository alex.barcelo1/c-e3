#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "../tabtools.c"

void eheure(char* h){
    int a = atoi(h);
    int he = a/3600;
    int m = (a-he*3600)/60;
    int s = a-he*3600-m*60;
    printf("%dh%dm%ds\n",he,m,s);
}

int sommes(char* s){
    int i=0;
    int r=0;
    while (*(s+i)!='\0')
    {
        r+=*(s+i);
        i+=1;
    }
    return r;
}

void populate(int* tab, int t){
    int i;
    for (i=0; i<t; i++){
        scanf("%d",(tab+i));
    }
}

int main(int argc, char *argv[])
{
    
    /*Pour demander l'arg au runtime si il n'est pas passé en argument */
    char arg[10];
    if(argc==1 || atoi(argv[1])!= atof(argv[1]) ){
        fgets(arg,10,stdin); 
    }else{
        strcpy(arg, argv[1]);
    }
    
    /*PARTIE 1*/
    eheure(arg);
    printf("%d\n",sommes("Test"));
    /* voir ../tabtools.c */
    int TABLEAUA
    pt(monTableau,t);
    int tab[10];
    populate(tab, 10);/*Fonctionne peu importe la taille du tab*/
    
    /*PARTIE 2*/
    /* voir ../tabtools.c */
    swap(tab,(tab+1));

    return 0;
}

