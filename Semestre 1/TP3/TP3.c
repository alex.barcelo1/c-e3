#include <stdio.h>
#define SIZE 50 

void ex3(int* t,int s){
    int max=t[0];
    int i;
    for(i=1;i<s;i++){
        if (t[i]>max)
        {
            max=t[i];
        }
    }
    printf("%d\n",max);
}

void pti(int* t,int s){
    int i;
    for(i=0;i<s;i++){
        printf("%d : %d\n",i,t[i]);
    }
}

void copie(int* s,int* c,int ts, int tc){
    int i;
    if (tc<ts){printf("Attention le tableau cible est trop petit, le tableau source sera copié partiellement\n");}
    if (tc>ts){printf("Attention le tableau source est trop petit, le tableau cible sera que partiellement une copie de la source\n");}
    for(i=0;i<ts&&i<tc;i++){
        c[i]=s[i];
        /*printf("i : %d // ci : %d // si : %d\n",i,c[i],s[i]);*/
    }

}

void copie2(int* s1,int* s2,int* c,int ts1,int ts2,int tc){
    if(ts1+ts2>tc){
        printf("Le tableau cible est trop petit");return;
    }
    copie(s1,c,ts1,tc);
    copie(s2,&c[ts1],ts2,tc-ts1);
    printf("Les 2 tablo ont étés collés dans le 3eme\n");
}

void cs(char* s){
    int cmp=0;
    while(s[cmp]!='\0'){
        cmp++;
    }
    printf("%d\n",cmp);
}

void plp(char* s, char* s2){
    int cmp=0;
    while(s[cmp]!='\0'&&s2[cmp]!='\0'){
        if(s[cmp]!=s2[cmp]){printf(" \n");return;}
        printf("%c",s[cmp]);
        cmp++;
    }
    
}

int main(int argc, char const *argv[])
{
    int tab [SIZE];
    int tab2[10];
    int tab3[100];
    char* s="Hello guys how are you";
    char* s2="Hello guys how are yall";
    int i;
    for(i=0;i<SIZE;i++){
        tab[i]=i+50;
    }
    for(i=0;i<10;i++){
        tab2[i]=i+10;
    }
    for(i=0;i<100;i++){
        tab3[i]=0;
    }
    tab[30]=1000;
    
    ex3(tab,SIZE);
    copie(tab,tab2,SIZE,10);
    copie2(tab,tab2,tab3,SIZE,10,100);
    pti(tab3,100);
    cs(s);
    plp(s,s2);
    return 0;
}
