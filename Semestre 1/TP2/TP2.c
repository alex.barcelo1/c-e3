﻿/*gcc -ansi -pedantic -Wall exo.c -o executable*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void sl(){printf("______________________________\n\n");}

void affint(int a, int b, int pas){
	int i;	
	/*Si les params sont dans le mauvais sens on les remet dans le bon*/		
	int cmp=0;
	if(a>b){int temp=a;a=b;b=temp;}	
	for(i=a;i<=b;i++){
		if(cmp%pas==0){cmp=0;printf("\n");}	
		cmp++;
		printf("%d	",i);
	}
	printf("\n");
}

void affintw(int a, int b, int pas){
	/*Si les params sont dans le mauvais sens on les remet dans le bon*/		
	int cmp=0;
	if(a>b){int temp=a;a=b;b=temp;}	
	while(a<=b){
		if(cmp%pas==0){cmp=0;printf("\n");}	
		cmp++;
		printf("%d	",a);
		a++;
	}
	printf("\n");
}

int somme(int a, int b){
	int res=0;
	if(a>b){int temp=a;a=b;b=temp;}
	while(a<=b){
		res+=a;
		a++;
	}	
	return res;
}

void affex(){
	affint(1,10,1);
}

int Exo3(int a){
/*Attention : retourne un int donc Exo3(100)=001=1*/
	int umd = 1; /*upper multiple de 10*/
	int res=0;
	int mul=1;
	int temp;
	/*recherche du multiple de 10 < a*/
	while (umd < a){
		umd=umd*10;
	}
	if(umd != a){		
		umd=umd/10;
	}
	/* a/umd=premier chiffre de a donc dernier chiffre de res*/
	while(umd>0){
		temp=a/umd;
		res+=temp*mul;
		a=a%umd;
		umd=umd/10;		
		mul=mul*10;	
	}

	return res;
}

char mtM(char c){
	
	int d = 'A'- 'a';
	char r = c+d;	
	if(c>'A' && c<'Z'){return c;}
	return r;
}

char Mtm(char c){
	int d = 'A'- 'a';
	char r = c-d;	
	if(c>'a' && c<'z'){return c;}	
	return r;
}


void Exo4(char a,char b){
	int ts=0;
	a=mtM(a);b=mtM(b);
	if(a>b){int temp=a;a=b;b=temp;}
		
	while(a<=b){
		char c;
		if (ts==0){c=Mtm(a);}else{c=mtM(a);} 
		printf("%c",c);
		a++;
		ts=(ts+1)%2;	
	}
	printf("\n");
}




void handle_var_1(int* a, int* b, int* c, int argc, char* argv[]){
	if(argc==1){
		printf("Borne 1 : \n");
		scanf("%d",a);
		printf("Borne 2 : \n");
		scanf("%d",b);
		printf("Pas : \n");
		scanf("%d",c);
	}

	if(argc==2){
		*b=atoi(argv[1]); /*atoi("5")=5*/
		*a=0;
		*c=10;
	}

	if(argc==3){
		printf("Pas : \n");
		scanf("%d",c);
		*a=atoi(argv[1]);*b=atoi(argv[2]);
	}

	if(argc==4){
		*a=atoi(argv[1]);*b=atoi(argv[2]);*c=atoi(argv[3]);
	}
}

void handle_var_2(int* a, int* b, int argc, char* argv[]){
	if (argc == 1){
		printf("Borne 1 : \n");
		scanf("%d",a);
		printf("Borne 2 : \n");
		scanf("%d",b);	
	}

	if (argc == 2){
		printf("Borne 2 : \n");
		scanf("%d",b);	
		*a=atoi(argv[1]);
	}
	
	if (argc == 3){
		*a=atoi(argv[1]);
		*b=atoi(argv[2]);	
	}
}

void handle_var_3(int* p,int argc, char* argv[]){
	if(argc==1){printf("Entrez le nombre à afficher à l'envers : ");scanf("%d",p);}
	if(argc==2){*p=atoi(argv[1]);}
}


/*LE SCANF EST SUPER CHELOU AVEC LES CHARS --> " %c" et ca marche (espace vide avant)*/
void handle_var_4(char* a, char* b, int argc, char* argv[]){
	if (argc == 1){
		printf("Borne 1 : \n");
		scanf(" %c",a);
		printf("Borne 2 : \n");
		scanf(" %c",b);	
	}

	if (argc == 2){
		printf("Borne 2 : \n");
		scanf(" %c",b);
		/*argv[1] est une string donc contient la valeur entrée et \0 on le convertit donc en char*/	
		*a=argv[1][0];
	}
	
	if (argc == 3){
		*a=argv[1][0];
		*b=argv[2][0];	
	}
}


int main(int argc, char* argv[]){
	int exo;
	printf("Quel exo ? : \n");		
	scanf("%d",&exo);

	/*EXO 1*/	
	if (exo==1){
		int param1,param2,param3;	
		int* p1=&param1;
		int* p2=&param2;
		int* p3=&param3;
		printf("EXO 1 : \n");
		handle_var_1(p1,p2,p3,argc,argv);
		affintw(param1,param2,param3);
		sl();
	}
	
	/*EXO 2*/	
	if (exo==2){
		int a,b;
		int* pa=&a;
		int* pb=&b;
		handle_var_2(pa,pb,argc,argv);
		printf("%d\n",somme(a,b));
		sl();
	}
	
	/*EXO3*/
	if(exo==3){
		int vex3;int* pvex3=&vex3;
		handle_var_3(pvex3,argc,argv);
		printf("%d\n",Exo3(vex3));			
		sl();
	}
	
	/*EXO4*/
	if(exo==4){
		char v1,v2;
		char* p1 = &v1;
		char* p2 = &v2;
		handle_var_4(p1,p2,argc,argv);
		Exo4(v1,v2);		
		sl();
	}

	return 0;
}
