#include "../tabtools.c"

void max_and_count(int* max,int* occ,int* tab,int t){
    int i;
    *occ = 0;
    *max=max_value(tab,t);
    for(i=0;i<t;i++){
        if(*max==*(tab+i)){
            (*occ)++;
        }
    }

}

void better_max_and_count(int* max,int* occ,int* tab,int t){
    int i;
    *occ=1;
    *max=*tab;
    for(i=0;i<t;i++){
        if(*max<*(tab+i)){
            *max=*(tab+i);
            *occ=0;
        }
        if(*max==*(tab+i)){
            (*occ)++;
        }
    }
}

void brutal_count_occ(int* tab,int* empty, int t){
    int i;
    for(i=0;i<t;i++){
        *(empty+i)=count(tab,*(tab+i),t);
    }
}

void brutal_count_occ_str(char* str, int* empty){
    int i;
    for(i=0;*(str+i)!='\0';i++){
        *(empty+i)=countchar(str,*(str+i));
    }
}

int main(int argc, char const *argv[])
{
    int TABLEAUMOM
    int empty[11];
    char* st="jesuisunestringquitestedestrucs";
    int emptys[6]={1,2,3,4,5,6};
    int max,occ;
    better_max_and_count(&max,&occ,montableau,t);
    printf("la valeur max est %d et elle apparait %d fois\n",max,occ);
    brutal_count_occ(montableau, empty, t);
    brutal_count_occ_str(st, emptys); 
    pt(empty,t);
    pt(emptys, strlen(st));
    return 0;
}