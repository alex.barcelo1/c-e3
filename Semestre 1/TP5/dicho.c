#include "../tabtools.c"

int place_premier(int* tab,int t){
    int x=*tab;
    int i;
    int ppg=0;

    for(i=1;i<t;i++){
        if(*(tab+i)>x){
            ppg=i;
            break;
        }
    }

    if(ppg==0){
        ppg=t;
    }
        
    for(i=ppg+1;i<t;i++){
        if(*(tab+i)<=x){
            swap((tab+i),(tab+ppg));
            ppg++;
        }
    }
    
    swap(tab,(tab+ppg-1));
    return ppg-1; 
}


void tri_dicho(int *tab,int t){ 
    if(t<=1){return;}
    int p = place_premier(tab,t);
    tri_dicho(tab, p);
    tri_dicho((tab+p+1),t-1-p);
}

int main(int argc, char const *argv[])
{
    int BTABLEAUA; //monTableau[5] = {3,2,5,4,1}
    tri_dicho(monTableau,t);
    pt(monTableau,t);
    return 0;
}