#include "../tabtools.c"

void bubblesort(int* tab, int t){
    int i,j;
    for(i=t-1;i>=1;i--){
        for(j=0;j<=i;j++){
            if (*(tab+j+1)<*(tab+j)){
                swap((tab+1+j),tab+j);
            }
        }
    }
}

void betterbubblesort(int* tab, int t){
    int i,j;
    bool sorted;
    for(i=t-1;i>0;i--){
        sorted=true;
        for(j=0;j<i;j++){
            if(*(tab+j+1)<*(tab+j)){
                swap((tab+j+1),(tab+j));
                sorted=false;
            }
        }
        if(sorted){
            return;
        }
    }
}

int main(int argc, char const *argv[])
{
    int BTABLEAUA
    pt(monTableau,t);
    betterbubblesort(monTableau,t);
    pt(monTableau,t);
    return 0;
}
