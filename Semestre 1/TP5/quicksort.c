#include "../tabtools.c"

int part(int* tab, int p, int d, int pivot){
    int i;
    int j=p;
    swap((tab+pivot),(tab+d));
    for(i=p;i<d;i++){
        if(*(tab+i)<=*(tab+d)){
            swap(tab+i,tab+j);
            j++;
        }
    }
    swap(tab+j,tab+d);
    return j;
}

void quicksort(int* tab, int p,int d){
    if(p<d){
        int pivot=(p+d)/2;
        pivot=part(tab, p, d, pivot);
        quicksort(tab, p, pivot-1);
        quicksort(tab, pivot+1, d);
    }
}

int main(int argc, char const *argv[])
{
    int BTABLEAUA;
    pt(monTableau,t);
    quicksort(monTableau,0,t);
    pt(monTableau,t);
    return 0;
}
